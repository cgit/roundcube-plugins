<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/codemirror_ui/localization/fi_FI.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Markku Virtanen - 01/12/2015 08:27:32
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Helperlisäosa koodin muokkaamiseen';
$labels['plugindescription'] = 'Kevyt helper lisäosa koodin muokkaukseen';

?>