<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/settings/localization/de_DE.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: myroundcube@mail4us.net - 01/12/2015 09:37:42
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Einstellungen';
$labels['plugindescription'] = 'Diese Hilfserweiterung wird von einigen anderen Erweiterungen benötigt (z.B. alle hmail_* Erweiterungen).';
$labels['menu'] = 'Menü';
$labels['managefolders'] = 'Ordner verwalten';
$labels['account'] = 'Konto-Einstellungen';
$labels['classic'] = 'Standard';
$labels['larry'] = 'Larry';
$labels['remotefolders'] = 'Spezialordner';
$labels['myroundcube'] = 'MyRoundCube';
$labels['serversettings.serversettings'] = 'Weitere Kontoeinstellungen';
$labels['serversettings.description'] = 'Weitere Kontoeinstellungen';

?>