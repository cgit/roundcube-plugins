<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/settings/localization/es_ES.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Yoni - 01/12/2015 10:28:29
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Configuración';
$labels['plugindescription'] = 'Este es un complemento de ayuda. Es requerido por otros complemento (p.e. todos los complementos hmail_* ).';
$labels['menu'] = 'Menú';
$labels['managefolders'] = 'Administrar carpetas';
$labels['account'] = 'Administrar cuentas';
$labels['classic'] = 'Por defecto';
$labels['larry'] = 'Larry';
$labels['remotefolders'] = 'Carpetas especiales';
$labels['myroundcube'] = 'MyRoundCube';
$labels['serversettings.serversettings'] = 'Otras configuraciones de cuenta';
$labels['serversettings.description'] = 'Otras configuraciones de cuenta';

?>