<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/settings/localization/fr_FR.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Olivier Zolli - 01/20/2015 17:59:37
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Paramètres';
$labels['plugindescription'] = 'Ce plugin est nécessaire pour le bon fonctionnement d\'autres plugins (par exemple tous les hmail* plugins)';
$labels['menu'] = 'Menu';
$labels['managefolders'] = 'Gestion des dossiers';
$labels['account'] = 'Administration du compte';
$labels['classic'] = 'Classic';
$labels['larry'] = 'Larry';
$labels['remotefolders'] = 'Dossiers spéciaux';
$labels['myroundcube'] = 'MyRoundCube';
$labels['serversettings.serversettings'] = 'Autres paramètres de compte';
$labels['serversettings.description'] = 'Autres paramètres de compte';

?>