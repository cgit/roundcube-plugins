<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/settings/localization/fi_FI.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Markku Virtanen - 01/15/2015 09:52:40
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Asetukset';
$labels['plugindescription'] = 'Tämä on helper lisäosa. Muut lisäosat käyttävät tätä lisäosaa (esim. kaikki hmail_* lisäosat).';
$labels['menu'] = 'Valikko';
$labels['managefolders'] = 'Hallinnoi kansioita';
$labels['account'] = 'Tilin hallinta';
$labels['classic'] = 'Oletus';
$labels['larry'] = 'Larry';
$labels['remotefolders'] = 'Erikoiskansiot';
$labels['myroundcube'] = 'MyRoundCube';
$labels['serversettings.serversettings'] = 'Muut tilin asetukset';
$labels['serversettings.description'] = 'Muut tilin asetukset';

?>