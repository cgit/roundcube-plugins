<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/settings/localization/cs_CZ.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: tachec - 01/20/2015 12:34:00
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Nastavení';
$labels['plugindescription'] = 'Toto je pomocný doplněk. Je využíván několika dalšími doplňky (např. všemi  hmail_* doplňky).';
$labels['menu'] = 'Menu';
$labels['managefolders'] = 'Správa složek';
$labels['account'] = 'Administrace účtu';
$labels['classic'] = 'Výchozí';
$labels['larry'] = 'Larry';
$labels['remotefolders'] = 'Speciální složky';
$labels['myroundcube'] = 'MyRoundCube';
$labels['serversettings.serversettings'] = 'Další nastavení účtu';
$labels['serversettings.description'] = 'Další nastavení účtu';

?>