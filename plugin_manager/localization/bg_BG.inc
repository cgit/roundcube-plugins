<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/plugin_manager/localization/bg_BG.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2013, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Alkin Yusufov - 05/03/2013 23:01:23
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Управление на добавки';
$labels['plugindescription'] = 'Управлението на добавки дава възможност за активиране и/или деактивиране на добавки за всеки потребителски профил поотделно, като същевременно дава възможност на администраторите да следят последните промени, актуалните добавки, поправки, централизирани изтегляния, документация и препоръки. Управлението на добавки не променя файловата система и не инсталира добавки автоматично.';
$labels['plugin_manager_title'] = 'Управление на добавки';
$labels['plugin_manager_center'] = 'Център за управление на добавки';
$labels['updatepm'] = 'Налична е нова версия на Управление на добавки.';
$labels['updatepmrequired'] = 'Необходима е актуализация на Управление на добавки.';
$labels['downloadnow'] = 'Изтегли сега';
$labels['calendaring'] = 'Календари';
$labels['misc'] = 'Други';
$labels['downloads'] = 'изтегляния';
$labels['issue'] = 'Проблем';
$labels['submitissue'] = 'Докладвай проблем.<br><br>Моля, проверете логовете си и предоставете уместна информация в съобщението.<br><br><b>ЗАБЕЛЕЖКА</b>: Изисква се профил в Google.';
$labels['showall'] = 'Покажи всички добавки';
$labels['hideuptodate'] = 'Покажи само актуализациите';
$labels['connectionerror'] = 'Няма отговор от отдалечения сървър при опит за връзка.';
$labels['trylater'] = 'Моля, опитайте по-късно ...';
$labels['translate'] = 'Преведи на тази добавка';
$labels['restoredefaults'] = 'Възстанови стандартните';
$labels['checkall'] = 'Маркирай всички';
$labels['uncheckall'] = 'Размаркирай всички';
$labels['toggle'] = 'Превключи избора';
$labels['status'] = 'Статус';
$labels['globalplugins'] = 'Глобални добавки';
$labels['backend'] = 'Сървърни добавки';
$labels['messagesmanagement'] = 'Управление на съобщения';
$labels['furtherconfig'] = 'Желаете ли да настроите тази добавка сега?';
$labels['uninstall'] = 'Вие ще деактивирате тази добавка. Желаете ли да изтриете съхранените настройки?';
$labels['uninstallconfirm'] = 'Вие ще деактивирате тази добавка. ПРЕДУПРЕЖДЕНИЕ: Изберете »ИЗКЛЮЧВАНЕ« за деактивиране на тази добавка, съхранявайки данните и настройките, записани на нашия сървър, ако смятате да ги използвате отново в бъдеще. Изберете »ПРЕМАХВАНЕ«, ако искате всички данни и настройки , управлявани от тази добавка, да бъдат изтрити от нашите бази данни. Обърнете внимание, че това действие е необратимо.';
$labels['areyousure'] = 'Това действие е необратимо. Наистина ли сте сигурни?';
$labels['successfullydeleted'] = 'Настройките бяха премахнати успешно.';
$labels['successfullysaved'] = 'Успешен запис.';
$labels['errorsaving'] = 'Възникна грешка при записване.';
$labels['yes'] = 'Да';
$labels['no'] = 'Не';
$labels['disable'] = 'Изключване';
$labels['remove'] = 'Премахване';
$labels['savewarning'] = 'Желаете ли да запишете промените?';
$labels['plugin_manager_update'] = 'Актуализирай добавките';
$labels['authors_comments'] = 'Бележки на автора (без превод)';
$labels['requiredby'] = 'Добавката се изисква от';
$labels['requires'] = 'Добавката изисква';
$labels['recommended'] = 'Препоръчани добавки';
$labels['update_plugins'] = 'Изтегляне на добавки';
$labels['ziparchive'] = 'Изтегли Zip-архив';
$labels['demoaccount'] = 'Съжаляваме, няма изтегляне (Demo профил)';
$labels['serverversion'] = 'Инсталирана версия';
$labels['mirrorversion'] = 'Актуална версия';
$labels['mirrorhost'] = 'Сървър за актуализация (host)';
$labels['comments'] = 'Изисквания и Бележки за актуализация';
$labels['update_error'] = 'Разлика във версиите';
$labels['update_ok'] = 'Открита е последната версия. Не е нужна актуализация.';
$labels['update_edit'] = 'Добавката трябва да се преконфигурира или изисква корекции в базата данни. Моля, прегледайте Бележките за актуализация.';
$labels['servernewer'] = 'Инсталираната версия на добавката е по-нова от огледалната версия.';
$labels['orhigher'] = 'или по-висока';
$labels['rc_ok'] = 'Версията на Roundcube е актуална.';
$labels['update_update'] = 'Добавката е неактуална. Моля, актуализирайте! Имайте предвид Бележките за актуализация.';
$labels['noupdates'] = 'Няма избрани актуализации.';
$labels['rc_update'] = 'Версията на Roundcube е неактуална. Моля, актуализирайте!';
$labels['rc_uptodate'] = 'Версията на Roundcube е актуална.';
$labels['rc_newer'] = 'Инсталираната версия на Roundcube е по-нова от огледалната версия.';
$labels['nottested'] = 'Моля, използвайте добавките с повишено внимание. Ние не знаем дали хостваните допълнения работят с Roundcube версия %s.';
$labels['justunzip'] = 'Просто разопаковайте архива като припокриете съществуващата инсталация.';
$labels['guide'] = 'Прочети повече ...';
$labels['thirdparty'] = 'Това не е добавка на MyRoundcube.';
$labels['thirdpartywarning'] = 'Това е добавка на външен разработчик. Препоръчваме Ви да не го изтегляте от нашите сървъри. Най-добре я изтеглете от сайта на разработчика, за да сте сигурни, че имате последната версия, както и забележките и препоръките на автора.';
$labels['develsite'] = 'Изтегляне';
$labels['notinstalled'] = 'не е инсталиран';
$labels['notregistered'] = 'не е регистриран';
$labels['roundcubeurl'] = 'Изтегляне на Roundcube';
$labels['languageupdate'] = 'Налична е актуализация на езиков пакет.';
$labels['localizationfilesonly'] = 'Забележка: Изтеглянето съдържа само езикови пакети.';
$labels['donotregister'] = 'Не регистрирайте това допълнение. Зарежда се автоматично.';
$labels['register'] = 'Регистрирайте тази добавка в настройките на Plugin Manager или в ./config/config.inc.php <small>[$config[\'plugins\'] = array("<i>foo</i>", "<i>bar</i>");]</small>.';
$labels['customer_account'] = 'Клиентски профил';
$labels['customer_id'] = 'Клиентски номер';
$labels['invalid_customer_id'] = 'Невалиден клиентски номер';
$labels['invalid_credits'] = 'Невалидни кредити';
$labels['successfully_transferred'] = 'Кредитите бяха прехвърлени успешно';
$labels['merge'] = 'Обединяване на кредити';
$labels['credits'] = 'Кредити';
$labels['creditsupdated'] = 'Има промяна в кредитите след последното опресняване на страницата';
$labels['buynow'] = 'Купи кредити сега';
$labels['history'] = 'История на профила';
$labels['details'] = 'Подробности';
$labels['receipt'] = 'Разписка';
$labels['plugins'] = 'Допълнения';
$labels['clickhere'] = 'Натисни тук';
$labels['discardliabletopaycosts'] = 'Откажи платените изтегляния';
$labels['unchecknotinstalledplugins'] = 'Откажи изтеглянията на добавките, които не са инсталирани';
$labels['sum'] = 'Сума';
$labels['show'] = 'Покажи';
$labels['hide'] = 'Скрий';
$labels['view'] = 'виж';
$labels['expired'] = 'изтекъл';
$labels['terms'] = 'Условия';
$labels['pricelist'] = 'Изтегли цените';
$labels['forthisdownload'] = 'за това изтегляне';
$labels['remainingcredits'] = 'оставащи кредити';
$labels['initialdownload'] = 'Първоначално изтегляне';
$labels['keyfeatureaddition'] = 'Нова функционалност';
$labels['codeimprovements'] = 'Подобрения на кода';
$labels['servicenotavailable'] = 'Услугата не е достъпна в момента. Моля, опитайте по-късно!';
$labels['myrcerror'] = 'Услугите на MyRoundcube не са достъпни в момента.';
$labels['getnew'] = 'Заяви нов клиентски номер';
$labels['getnew_hint'] = 'Препоръчваме Ви често да подновявате клиентския си номер. Ако забележите подозрителна активност в профила си, незабавно подновете клиентския си номер.';
$labels['transfer'] = 'Прехвърляне на кредити';
$labels['submenuprefix'] = ' » ';
$labels['noplugindescription'] = 'Не е открито описание на допълнението.';
$labels['markbuttons_pluginname'] = 'Бутони за маркиране на писмата';
$labels['markbuttons_plugindescription'] = 'Добавя икони под списъка със съобщения за бързо маркиране на мейлите.';
$labels['keyboard_shortcuts_pluginname'] = 'Клавиши за бърз достъп';
$labels['keyboard_shortcuts_plugindescription'] = 'Изпълнение на команди с използване на клавишни комбинации.';
$labels['message_highlight_pluginname'] = 'Оцветяване на съобщения';
$labels['message_highlight_plugindescription'] = 'Оцветяване на съобщенията, които отговарят на определени критерии.';
$labels['google_contacts_pluginname'] = 'Контакти от Google';
$labels['google_contacts_plugindescription'] = 'Достъп до контактите Ви в Google.';
$labels['contextmenu_pluginname'] = 'Контекстно меню';
$labels['contextmenu_plugindescription'] = 'Добавяне на контекстно меню при кликване на десния бутон на мишката.';

?>