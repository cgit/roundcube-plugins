if(window.rcmail){
  rcmail.addEventListener('init', function(){
    // logo click (all Roundcube versions)
    if(rcmail.env.skin == 'larry'){
      $('#toplogo').attr('onclick', '');
      $('#toplogo').click(function(){
        $('.button-mail').click();
      });
      $('#toplogo').attr('style', 'cursor: pointer');
    }
  });

  // fix date alignment
  if(rcmail.env.task == 'mail' && !rcmail.env.framed && !rcmail.env.extwin){
    rcmail.addEventListener('listupdate', function(){
      $(window).trigger('resize');
    });
  }
}
