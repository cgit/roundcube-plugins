<?php
# 
# This file is part of Roundcube "db_config" plugin.
# 
# Your are not allowed to distribute this file or parts of it.
# 
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# Copyright (c) 2012 - 2015 Roland 'Rosali' Liebl - all rights reserved.
# dev-team [at] myroundcube [dot] net
# http://myroundcube.com
# 
$labels=array();$labels['pluginname']='Plugin paramètres de configuration en DB';$labels['plugindescription']='Configurer les plugins via une database au lieu de fichiers';