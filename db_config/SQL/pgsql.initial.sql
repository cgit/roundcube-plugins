CREATE TABLE IF NOT EXISTS db_config (
  id serial PRIMARY KEY,
  env varchar(255) DEFAULT NULL,
  conf text,
  admin varchar(128) NOT NULL
);

CREATE TABLE IF NOT EXISTS "system" (
  name varchar(64) NOT NULL PRIMARY KEY,
  value text
);

INSERT INTO "system" (name, value) VALUES ('myrc_db_config', 'initial');