CREATE TABLE IF NOT EXISTS 'db_config' (
  'id' INTEGER NOT NULL PRIMARY KEY ASC,
  'env' varchar(255) NOT NULL,
  'conf' text NOT NULL,
  'admin' varchar(128) NOT NULL
);

CREATE TABLE IF NOT EXISTS 'system' (
  name varchar(64) NOT NULL PRIMARY KEY,
  value text NOT NULL
);

INSERT INTO system (name, value) VALUES ('myrc_db_config', 'initial');