IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[db_config]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [db_config] (
  [id] int NOT NULL IDENTITY(1,1),
  [env] nvarchar(255) NOT NULL,
  [conf] nvarchar(MAX) NOT NULL,
  [admin] nvarchar(128) NOT NULL,
  PRIMARY KEY ([id])
)
GO

INSERT INTO [system] (name, value) VALUES ('myrc_db_config', 'initial')
GO
