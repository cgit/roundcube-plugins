ALTER TABLE `db_config` CHANGE `env` `env` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `db_config` CHANGE `conf` `conf` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `db_config` CHANGE `admin` `admin` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
UPDATE `system` SET `value`='initial|20131110|20141115' WHERE `name`='myrc_db_config';