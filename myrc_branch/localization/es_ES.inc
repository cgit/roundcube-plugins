<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/myrc_branch/localization/es_ES.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Yoni - 01/11/2015 17:51:55
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Rama de MyRoundcube';
$labels['plugindescription'] = 'Un complemento auxiliar para indicar otros complementos la rama de MyRoundcube que se usa.';

?>