<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/myrc_branch/localization/cs_CZ.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: tachec - 01/22/2015 08:03:27
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['pluginname'] = 'Větev MyRoundcube';
$labels['plugindescription'] = 'Pomocný doplněk pro informaci ostatním doplňkům o větvi MyRoundcube.';

?>