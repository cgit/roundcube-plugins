<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/libgpl/localization/cs_CZ.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: tachec - 01/22/2015 08:02:38
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['calendar_caldav'] = 'CalDAV kalendář';
$labels['calendar_kolab'] = 'Kolab kalendář';
$labels['calendar_database'] = 'lokální kalendář';
$labels['calendar_ical'] = 'iCAL kalendář';
$labels['calendar_google_xml'] = 'Google XML kalendář';
$labels['sync_interval'] = 'Synchronizovat každých';
$labels['minute_s'] = 'minut(y)';
$labels['tasks'] = 'Úkoly';
$labels['errorimportingtask'] = 'Nepodařilo se naimportovat úkoly';
$labels['treat_as_allday'] = 'Zobrazit událost jako celodenní pokud trvá déle než';
$labels['hours'] = 'Hodin';
$labels['movetotasks'] = 'Přesunout do úkolů';
$labels['movetocalendar'] = 'Přesunout do kalendáře';
$labels['emailevent'] = 'Poslat událost';
$labels['emailnote'] = 'Poslat poznámku';
$labels['emailtask'] = 'Poslat úkol';
$labels['movetonotes'] = 'Přesunout do poznámek';
$labels['quit'] = 'Ukončit';
$labels['eventaction'] = 'Akce události...';
$labels['googledisabled'] = 'Povolte prosím přístup k vašemu účtu Google (přejděte na "Nastavení" -> "Správa účtu" -> "Další nastavení účtu" a proveďte patřičná nastavení).';
$labels['googledisabled_redirect'] = 'Povolte prosím přístup k vašemu účtu Google.';
$labels['allowfreebusy'] = 'Povolit požadavek na obsazenost';
$labels['freebusy'] = 'Obsazenost';
$labels['unabletoadddefaultcalendars'] = 'Nelze přidat výchozí kalendář';
$labels['taskaction'] = 'Akce úkolu...';
$labels['due'] = ' ';
$labels['is_subtask'] = 'Dílčí úkol';
$labels['subscribe'] = 'Abyste mohli vytvářet úkoly, tak si je prosím povolte ve svém kalendáři.';
$labels['subscribed'] = 'Přihlášeno';
$labels['tags'] = 'Kategorie';
$labels['list'] = 'Kalendář';
$labels['editlist'] = 'Upravit kalendář';

?>