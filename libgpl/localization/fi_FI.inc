<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/libgpl/localization/fi_FI.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Markku Virtanen - 01/12/2015 07:37:20
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['calendar_caldav'] = 'CalDAV kalenteri';
$labels['calendar_kolab'] = 'Kolab kalenteri';
$labels['calendar_database'] = 'Paikallinen kalenteri';
$labels['calendar_ical'] = 'iCal kalenteri';
$labels['calendar_google_xml'] = 'Google XML kalenteri';
$labels['sync_interval'] = 'Synkronisointi joka';
$labels['minute_s'] = 'minuutti(a)';
$labels['tasks'] = 'Tehtävät';
$labels['errorimportingtask'] = 'Tehtävän tuonti epäonnistui';
$labels['treat_as_allday'] = 'Näytä tapahtumat koko päivän tapahtumana jos ne kestävät enemmän kuin';
$labels['hours'] = 'Tuntia';
$labels['movetotasks'] = 'Siirrä tehtäviin';
$labels['movetocalendar'] = 'Siirrä kalenteriin';
$labels['emailevent'] = 'Lähetä tapahtuma';
$labels['emailnote'] = 'Lähetä viesti';
$labels['emailtask'] = 'Lähetä tehtävä';
$labels['movetonotes'] = 'Siirrä muistioon';
$labels['quit'] = 'Poistu';
$labels['eventaction'] = 'Tapahtuman toiminto...';
$labels['googledisabled'] = 'Anna valtuutus Google tilille ("Asetukset" -> "Tilin hallinta" -> "Muut tilin asetukset")';
$labels['googledisabled_redirect'] = 'Anna valtuutus Google tilillesi';
$labels['allowfreebusy'] = 'Salli freebusy pyynnöt';
$labels['freebusy'] = 'Freebusy';
$labels['unabletoadddefaultcalendars'] = 'Oletuskalenterien lisäys epäonnistui';
$labels['taskaction'] = 'Tehtävän toiminto...';
$labels['due'] = 'valmistuu';
$labels['is_subtask'] = 'Alatehtävä';
$labels['subscribe'] = 'Tee tilaus kalenteriin jotta voit luoda tehtäviä.';
$labels['subscribed'] = 'Tilattu';
$labels['tags'] = 'Kategoriat';
$labels['list'] = 'Kalenteri';
$labels['editlist'] = 'Muokkaa kalenteria';

?>