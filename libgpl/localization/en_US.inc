<?php
$labels = array();
$labels['calendar_caldav'] = 'CalDAV Calendar';
$labels['calendar_kolab'] = 'Kolab Calendar';
$labels['calendar_database'] = 'Local Calendar';
$labels['calendar_ical'] = 'iCAL Calendar';
$labels['calendar_google_xml'] = 'Google XML Calendar';
$labels['sync_interval'] = 'Synchronization every';
$labels['minute_s'] = 'minute(s)';
$labels['tasks'] = 'Tasks';
$labels['errorimportingtask'] = 'Failed to import the task';
$labels['treat_as_allday'] = 'Show event as all-day if it takes more than';
$labels['hours'] = 'Hours';
$labels['movetotasks'] = 'Move to tasks';
$labels['movetocalendar'] = 'Move to calendar';
$labels['emailevent'] = 'Email event';
$labels['emailnote'] = 'Email note';
$labels['emailtask'] = 'Email task';
$labels['movetonotes'] = 'Move to notes';
$labels['quit'] = 'Quit';
$labels['eventaction'] = 'Event action...';
$labels['googledisabled'] = 'Please authorize access to your Google account (browse to <i>"Settings"</i> -> <i>"Account Administration" </i> -> <i>"Other account\'s settings"</i> and proceed).';
$labels['googledisabled_redirect'] = 'Please authorize access to your Google account.';
$labels['allowfreebusy'] = 'Allow freebusy requests';
$labels['freebusy'] = 'Freebusy';
$labels['unabletoadddefaultcalendars'] = 'Unable to add default calendars';
$labels['taskaction'] = 'Task action...';
$labels['due'] = 'due';
$labels['is_subtask'] = 'Subtask';
$labels['subscribe'] = 'Please subscribe to a calendar in order to create a task.';
$labels['subscribed'] = 'Subscribed';
$labels['tags'] = 'Categories';
$labels['list'] = 'Calendar';
$labels['editlist'] = 'Edit calendar';

?>