<?php
/*
 +-----------------------------------------------------------------------+
 | ./plugins/libgpl/localization/es_ES.inc
 |
 | Language file of MyRoundcube Plugins Bundle
 | Copyright (C) 2010-2015, Roland 'rosali' Liebl
 |
 +-----------------------------------------------------------------------+
 | Author: Yoni - 01/10/2015 10:46:25
 +-----------------------------------------------------------------------+
*/

$labels = array();
$labels['calendar_caldav'] = 'Calendario CalDAV';
$labels['calendar_kolab'] = 'Calendario Kolab';
$labels['calendar_database'] = 'Calendario Local';
$labels['calendar_ical'] = 'Calendario iCAL';
$labels['calendar_google_xml'] = 'Calendario Google XML';
$labels['sync_interval'] = 'Sincronización cada';
$labels['minute_s'] = 'minuto(s)';
$labels['tasks'] = 'Tareas';
$labels['errorimportingtask'] = 'No se pudo importar la tarea';
$labels['treat_as_allday'] = 'Mostrar evento como \'todo el día\' si toma más de';
$labels['hours'] = 'Horas';
$labels['movetotasks'] = 'Mover a tareas';
$labels['movetocalendar'] = 'Mover al calendario';
$labels['emailevent'] = 'Enviar evento por correo';
$labels['emailnote'] = 'Enviar nota por correo';
$labels['emailtask'] = 'Enviar tarea por correo';
$labels['movetonotes'] = 'Mover a notas';
$labels['quit'] = 'Salir';
$labels['eventaction'] = 'Acción de evento...';
$labels['googledisabled'] = 'Please authorize access to your Google account (browse to "Settings" -> "Account Administration" -> "Other account\'s settings" and proceed).  Por favor, autorizar el acceso a su cuenta de Google (vaya a "Configuración" -> "Administración de Cuenta" -> "Otros ajustes de cuentas" y proceda).';
$labels['googledisabled_redirect'] = 'Por favor, autorizar el acceso a su cuenta de Google.';
$labels['allowfreebusy'] = 'Permitir solicitudes Libre/Ocupado';
$labels['freebusy'] = 'Libre/Ocupado';
$labels['unabletoadddefaultcalendars'] = 'No se pudo agregar calendarios predeterminados';
$labels['taskaction'] = 'Acción de tarea...';
$labels['due'] = 'debido el';
$labels['is_subtask'] = 'Subtarea';
$labels['subscribe'] = 'Por favor, suscribirse a un calendario para poder crear tarea.';
$labels['subscribed'] = 'Suscrito';
$labels['tags'] = 'Categorías';
$labels['list'] = 'Calendario';
$labels['editlist'] = 'Editar calendario';

?>