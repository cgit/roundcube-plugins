<?php

/*
+-----------------------------------------------------------------------+
| language/fr_FR/labels.inc                                             |
|                                                                       |
| Language file of the RoundCube Webmail client                         |
| Copyright (C) 2008-2012, RoundQube Dev. - Switzerland                 |
| Licensed under the GNU GPL                                            |
|                                                                       |
+-----------------------------------------------------------------------+
| Author: Olivier Zolli - 01/11/2015                                    |
+-----------------------------------------------------------------------+

*/

$labels = array();
$labels['calendar_caldav'] = 'Agenda CalDAV';
$labels['calendar_kolab'] = 'Agenda Kolab';
$labels['calendar_database'] = 'Agenda local';
$labels['calendar_ical'] = 'Agenda iCAL';
$labels['calendar_google_xml'] = 'Agenda Google XML';
$labels['sync_interval'] = 'Synchronisation chaque';
$labels['minute_s'] = 'minute(s)';
$labels['tasks'] = 'Tâches';
$labels['errorimportingtask'] = 'Echec d\'importation de la tâche';
$labels['treat_as_allday'] = 'Montrer l\'évènement comme "Toute la journée" s\'il dure plus de';
$labels['hours'] = 'Heures';
$labels['movetotasks'] = 'Déplacer vers les tâches';
$labels['movetocalendar'] = 'Déplacer vers l\'agenda';
$labels['emailevent'] = 'Evènement courriel';
$labels['emailnote'] = 'Evènement note';
$labels['emailtask'] = 'Evènement tâche';
$labels['movetonotes'] = 'Déplacer vers les notes';
$labels['quit'] = 'Quitter';
$labels['eventaction'] = 'Action d\'évènement';
$labels['googledisabled'] = 'Veuillez autoriser l\'accès à votre compte Google (<i>"Paramètres"</i> -> <i>"Administration du compte"</i> -> <i>"Autres paramètres du compte"</i>';
$labels['googledisabled_redirect'] = 'Veuillez autoriser l\'accès à votre compte Google';
$labels['allowfreebusy'] = 'Autoriser les requêtes Libre/Occupé';
$labels['freebusy'] = 'Libre/Occupé';
$labels['unabletoadddefaultcalendars'] = 'Impossible d\'ajouter les agendas par défaut';
$labels['taskaction'] = 'Action de tâche';
$labels['due'] = 'requis';
$labels['is_subtask'] = 'Sous-tâche';
$labels['subscribe'] = 'Veuillez vous inscrire à un agenda pour créer une tâche';
$labels['subscribed'] = 'Inscrit';
$labels['tags'] = 'Catégories';
$labels['list'] = 'Agenda';
$labels['editlist'] = 'Editer l\'agenda';

?>